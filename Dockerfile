FROM python:3.6-alpine

ADD . /workdir

WORKDIR /workdir

RUN pip install -r requirements.txt

ENTRYPOINT cd mysite && python manage.py runserver 0.0.0.0:6789
